#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 19 17:13:40 2021

"""

from __future__ import absolute_import
import argparse

PROGRAM_DESCRIPTION = """
Write a program (pymetacline/scripts/select_fasta.py) that reads the fasta file
located in the data/fasta folder of pymetacline package and select a user-defined
set of sequences identifiers (argument -seq that accept a comma separated list of
sequence identifiers or a a set of identifiers using a positional argument).
The program should implement a CLI and should be tested with pylint.
"""

def create_parser() :
    """
    Creation and definition of the arguments used (incoming and outgoing).
    """

    parser = argparse.ArgumentParser(add_help = True,
                                     description = PROGRAM_DESCRIPTION)

    parser.add_argument('-i','--inputfile',
                        help = "Path to the file",
                        type = argparse.FileType("r"),
                        metavar= "FASTA",
                        required = True)

    parser.add_argument('-id_seq', '--identifiers',
                        help = "Entry sequence identifiers",
                        type = str,
                        required = True)

    parser.add_argument('-o', '--outputfile',
                        help = "Output file",
                        type = argparse.FileType('w'),
                        metavar="FASTA",
                        required = True)
    return parser


def search_seq_ids(inputfile,identifiers,outputfile) :
    """
    The function returns a dictionary containing in key the identifiers
    and value the sequences.
    @param inputFile : fasta file
    @param id_list : search fasta ids
    @param out : output file
    """
    dico_fasta = {}
    seq_id = ""
    seq = ""
    keep_fasta = True
    # keep_fasta is a boolean variable that allows us
    # to apply commands according to its value
    for line in inputfile :
        if not line.startswith('\n') :
            if line.startswith(">") : # if the line starts with ">"
                seq_id = line[1:-1] # we retrieve the identifier
                keep_fasta = seq_id in identifiers # replaces an if/else condition by a boolean
            else : # if the line doesn't starts with ">"
                if keep_fasta : # if keep_fasta == True :
                    seq = line[0:-1]
                    dico_fasta[seq_id] = seq
    for seq_id in dico_fasta :
        outputfile.write(">" + seq_id + "\n" + dico_fasta[seq_id] + "\n\n")
    inputfile.close()
    outputfile.close()


def main() :
    """
    Main function which takes into account the previously
    defined arguments and processes them by calling on the other
    processing functions.
    """
    parser = create_parser()
    args = parser.parse_args()
    print(args)
    search_seq_ids(args.inputfile,args.identifiers, args.outputfile)


if __name__ == "__main__" :
    main()
