#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 15:34:12 2021

"""

from __future__ import absolute_import
import argparse

PROGRAM_DESCRIPTION ='''
Write a program (pymetacline/scripts/select_fasta.py) that reads the fasta file
located in the data/fasta folder of pymetacline package and select a user-defined
set of sequences identifiers (argument -seq that accept a comma separated list of
sequence identifiers or a a set of identifiers using a positional argument).
The program should implement a CLI and should be tested with pylint.
'''

def create_parser():
    """
    Creation and definition of the arguments
    used (incoming and outgoing).
    """
    parser = argparse.ArgumentParser(add_help=True,
                                description=PROGRAM_DESCRIPTION)
    parser.add_argument('-i', '--inputfile',
                   help="Path to the Fasta file.",
                   default=None,
                   metavar="FASTA",
                   type=argparse.FileType('r'),
                   required=True)

    parser.add_argument('-id_seq','--identifier',
                        nargs='+',
                        help="Enter identifiers sequences.",
                        default=None,
                        metavar="",
                        type=str,
                        required=True)

    parser.add_argument('-o', '--outfile',
                        help="Output file.",
                        default=None,
                        metavar="",
                        type=argparse.FileType('w'),
                        required=True)

    return parser

def recovery_id_seq(arg_i, arg_s, arg_o):
    """
    The function takes as input a fasta file containing a set of sequences,
    one or more identifiers and browses this file in order to output in a new
    document the identifier sought as well as its sequence.
    """
    iden = arg_s
    recup_id = str(iden).replace('[','').replace(']','').replace("'","")
    each_id = recup_id.split(",")
    for lines in arg_i :
        seq = next(arg_i)
        for k in each_id:
            if k in lines :
                content = lines + seq + '\n'
                arg_o.write(content)
            else :
                pass
    arg_o.close()

def main():
    """
    Main function which takes into account the previously
    defined arguments and processes them by calling on the other
    processing functions.
    """
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    print(args)
    recovery_id_seq(args['inputfile'], args['identifier'], args['outfile'])

if __name__ == "__main__":
    main()
